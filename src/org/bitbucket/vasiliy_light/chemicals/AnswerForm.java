package org.bitbucket.vasiliy_light.chemicals;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Form;

public class AnswerForm extends Form {
    
    private final Command showMainForm = new Command(Ini.ITEM4, Command.SCREEN, 0);

    public AnswerForm(String title, String answer, CommandListener cmdListener) {
        super(title);
        append(answer);
        addCommand(showMainForm);
        setCommandListener(cmdListener);
    }

    public Command getShowMainForm() {
        return showMainForm;
    }
    
}
