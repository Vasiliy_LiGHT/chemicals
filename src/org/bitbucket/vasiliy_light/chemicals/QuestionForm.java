package org.bitbucket.vasiliy_light.chemicals;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Form;

public class QuestionForm extends Form {

    private final Command pushYes = new Command(Ini.YES, Command.SCREEN, 0);
    private final Command pusnNo = new Command(Ini.NO, Command.SCREEN, 0);
    private final Command showMainForm = new Command(Ini.ITEM4, Command.SCREEN, 1);

    public QuestionForm(String title, String question, CommandListener cmdListener) {
        super(title);
        append(question);
        addCommand(pushYes);
        addCommand(pusnNo);
        addCommand(showMainForm);
        setCommandListener(cmdListener);
    }

    public Command getPushYes() {
        return pushYes;
    }

    public Command getPushNo() {
        return pusnNo;
    }

    public Command getShowMainForm() {
        return showMainForm;
    }

}