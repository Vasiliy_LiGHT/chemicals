package org.bitbucket.vasiliy_light.chemicals;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.midlet.MIDlet;

public class ChemicalsRun extends MIDlet implements CommandListener {

    private final Command showQuestionForm = new Command(Ini.ITEM1, Command.SCREEN, 1);
    private final Command showAutorForm = new Command(Ini.ITEM2, Command.SCREEN, 2);
    private final Command exitApp = new Command(Ini.ITEM3, Command.EXIT, 0);
    private final Form mainForm = new Form(Ini.F_MAIN);
    private final AnswerForm formAutor = new AnswerForm(Ini.F_ABOUT, Ini.F_ABOUTTEXT, this);
    private final AnswerForm a1 = new AnswerForm(Ini.ANSWER, Ini.AF1_TEXT, this);
    private final AnswerForm a2 = new AnswerForm(Ini.ANSWER, Ini.AF2_TEXT, this);
    private final AnswerForm a3 = new AnswerForm(Ini.ANSWER, Ini.AF3_TEXT, this);
    private final AnswerForm a4 = new AnswerForm(Ini.ANSWER, Ini.AF4_TEXT, this);
    private final AnswerForm a5 = new AnswerForm(Ini.ANSWER, Ini.AF5_TEXT, this);
    private final QuestionForm q1 = new QuestionForm(Ini.QUESTION + Ini.F1_NUM, Ini.QF1_TEXT, this);
    private final QuestionForm q2 = new QuestionForm(Ini.QUESTION + Ini.F2_NUM, Ini.QF2_TEXT, this);
    private final QuestionForm q3 = new QuestionForm(Ini.QUESTION + Ini.F3_NUM, Ini.QF3_TEXT, this);
    private final QuestionForm q4 = new QuestionForm(Ini.QUESTION + Ini.F4_NUM, Ini.QF4_TEXT, this);
    private final Display mainDisplay;

    public ChemicalsRun() {
        mainDisplay = Display.getDisplay(this);
        initMainForm();
        mainDisplay.setCurrent(mainForm);
    }

    public void startApp() {
        mainDisplay.setCurrent(mainForm);
    }

    protected void destroyApp(boolean unconditional) {
    }

    protected void pauseApp() {
    }

    private void initMainForm() {
        mainForm.append(Ini.F_MAINTEXT);
        mainForm.addCommand(showQuestionForm);
        mainForm.addCommand(showAutorForm);
        mainForm.addCommand(exitApp);
        mainForm.setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == showAutorForm) {
            mainDisplay.setCurrent(formAutor);
        }
        if (c == showQuestionForm) {
            mainDisplay.setCurrent(q1);
        }
        if (c == exitApp) {
            destroyApp(false);
            notifyDestroyed();
        }
        if (d == formAutor) {
            if (c == formAutor.getShowMainForm()) {
                mainDisplay.setCurrent(mainForm);
            }
        }
        if (d == a1) {
            if (c == a1.getShowMainForm()) {
                mainDisplay.setCurrent(mainForm);
            }
        }
        if (d == a2) {
            if (c == a2.getShowMainForm()) {
                mainDisplay.setCurrent(mainForm);
            }
        }
        if (d == a3) {
            if (c == a3.getShowMainForm()) {
                mainDisplay.setCurrent(mainForm);
            }
        }
        if (d == a4) {
            if (c == a4.getShowMainForm()) {
                mainDisplay.setCurrent(mainForm);
            }
        }
        if (d == a5) {
            if (c == a5.getShowMainForm()) {
                mainDisplay.setCurrent(mainForm);
            }
        }
        if (d == q1) {
            if (c == q1.getPushYes()) {
                mainDisplay.setCurrent(q3);
            }
            if (c == q1.getPushNo()) {
                mainDisplay.setCurrent(q2);
            }
            if (c == q1.getShowMainForm()) {
                mainDisplay.setCurrent(mainForm);
            }
        }
        if (d == q2) {
            if (c == q2.getPushYes()) {
                mainDisplay.setCurrent(q4);
            }
            if (c == q2.getPushNo()) {
                mainDisplay.setCurrent(a2);
            }
            if (c == q2.getShowMainForm()) {
                mainDisplay.setCurrent(mainForm);
            }
        }
        if (d == q3) {
            if (c == q3.getPushYes()) {
                mainDisplay.setCurrent(a3);
            }
            if (c == q3.getPushNo()) {
                mainDisplay.setCurrent(a1);
            }
            if (c == q3.getShowMainForm()) {
                mainDisplay.setCurrent(mainForm);
            }
        }
        if (d == q4) {
            if (c == q4.getPushYes()) {
                mainDisplay.setCurrent(a4);
            }
            if (c == q4.getPushNo()) {
                mainDisplay.setCurrent(a5);
            }
            if (c == q4.getShowMainForm()) {
                mainDisplay.setCurrent(mainForm);
            }
        }
    }
}
